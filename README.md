# README #

### How do I get set up? ###
In order to run this program it's necessary to download and setup(via terminal) the GSL Library. 

(Ubundu 14.04)
1. The packages for GSL can be found here: 
   **https://launchpad.net/ubuntu/+source/gsl**
2. You can install just the library by using: 
   **apt-get install libgsl0ldbl**
3. You can also install the development package and binary using: 
   **apt-get install gsl-bin libgsl0-dev**
4. The resources/docs can also be installed using: 
   **apt-get install gsl-doc-info gsl-doc-pdf gsl-ref-html gsl-ref-psdoc**

(*depending on your setup you may need to use sudo before the apt-get command) 
Also, you have to include library's path in gcc command.